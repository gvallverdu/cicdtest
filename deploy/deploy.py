import pandas as pd
import numpy as np
import datetime


df = pd.read_csv("OV5-1.csv", sep=",", index_col=0, skiprows=3,
                 nrows=109, dtype=np.float,
                 parse_dates=True, engine='python')

date = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M")
print(df.head())

df[["Pt194", "In115"]].to_csv("Pt194.csv", header=date)
df[["Pt195", "In115"]].to_csv("Pt195.csv", header=date)

